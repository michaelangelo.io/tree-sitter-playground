mod highlight_core;

use highlight_core::{apply_highlights, Range};

use serde::{Deserialize, Serialize};
use serde_wasm_bindgen::{from_value, to_value};
use wasm_bindgen::prelude::*;
use web_sys::console;

// #[derive(Deserialize, Serialize, Debug)]
// struct Range {
//     start_line: usize,
//     start_col: usize,
//     end_line: usize,
//     end_col: usize,
// }

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(typescript_type = "Range")]
    pub type RangeType;
}

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(typescript_type = "Array<Range>")]
    pub type RangeArray;
}

#[wasm_bindgen(typescript_custom_section)]
const RANGESTYPE: &'static str = r#"
interface Range {
    start_line: number;
    start_col: number;
    end_line: number;
    end_col: number;
}
"#;

#[wasm_bindgen(typescript_custom_section)]
const RANGESARRAY: &'static str = r#"
type RangesArray = Array<Range>;
"#;

#[wasm_bindgen]
pub fn highlight(text: &JsValue, ranges: &JsValue) -> JsValue {

    let text: String = match from_value(text.clone()) {
        Ok(text) => text,
        Err(e) => {
            console::error_1(&format!("Error deserializing text: {:?}", e).into());
            return JsValue::from_str("");
        }
    };

    let ranges: Vec<Range> = match from_value(ranges.clone()) {
        Ok(ranges) => ranges,
        Err(e) => {
            console::error_1(&format!("Error deserializing ranges: {:?}", e).into());
            return JsValue::from_str("");
        }
    };

    console::log_1(&format!("Text: {}", text).into());
    console::log_1(&format!("Ranges: {:?}", ranges).into());

    let highlighted_text = apply_highlights(&text, &ranges);
    JsValue::from_str(&highlighted_text)
}

// fn apply_highlights(text: &str, ranges: &[Range]) -> String {
//     let mut result = String::new();
//     let mut last_end = 0;

//     for range in ranges {
//         console::log_1(&format!("Processing range: {:?}", range).into());
//         let start_index = compute_index(text, range.start_line, range.start_col);
//         let end_index = compute_index(text, range.end_line, range.end_col);

//         console::log_1(&format!("Start index: {}, End index: {}", start_index, end_index).into());

//         // Adjust end_index to be within text bounds if it exceeds text length
//         let end_index = if end_index > text.len() {
//             text.len()
//         } else {
//             end_index
//         };

//         result.push_str(&text[last_end..start_index]);
//         result.push_str(&format!(
//             "<span class=\"bg-yellow-200\">{}</span>",
//             &text[start_index..end_index]
//         ));
//         console::log_1(&format!("Last end: {}", last_end).into());
//         last_end = end_index;
//     }

//     result.push_str(&text[last_end..]);
//     result.replace("\n", "<br>")
// }

// fn compute_index(text: &str, line: usize, col: usize) -> usize {
//     let mut index = 0;
//     let mut current_line = 0;
//     let mut chars = text.chars().peekable();

//     while let Some(&c) = chars.peek() {
//         if current_line == line {
//             index += col;
//             break;
//         }
//         if c == '\n' {
//             current_line += 1;
//             if current_line <= line {
//                 index += 1;
//             }
//         } else {
//             index += 1;
//         }
//         chars.next();
//     }

//     // Handle cases where the end line/col is out of the text bounds
//     if current_line < line {
//         while let Some(c) = chars.next() {
//             if c == '\n' {
//                 current_line += 1;
//             }
//             index += 1;
//         }
//     }

//     index
// }

// #[cfg(test)]
// mod tests {
//     use super::*;

//     #[test]
//     fn test_compute_index() {
//         let text = "function foo() {\n  console.log('bar')\n}";
//         assert_eq!(compute_index(text, 0, 8), 8);
//         assert_eq!(compute_index(text, 1, 2), 19); // Adjusted to match 0-based indexing
//         assert_eq!(compute_index(text, 2, 0), 38);
//     }

//     #[test]
//     fn test_apply_highlights() {
//         let text = "function foo() {\n  console.log('bar')\n}";
//         let ranges = vec![
//             Range { start_line: 0, start_col: 8, end_line: 0, end_col: 11 },
//             Range { start_line: 1, start_col: 2, end_line: 1, end_col: 9 },
//         ];
//         let expected = "function <span class=\"bg-yellow-200\">foo()</span> {\n  <span class=\"bg-yellow-200\">console</span>.log('bar')\n}";
//         assert_eq!(apply_highlights(text, &ranges), expected.replace("\n", "<br>"));
//     }

//     #[test]
//     fn test_apply_highlights_multiline() {
//         let text = "function foo() {\n  console.log('bar')\n}";
//         let ranges = vec![
//             Range { start_line: 0, start_col: 0, end_line: 1, end_col: 9 },
//         ];
//         let expected = "<span class=\"bg-yellow-200\">function foo() {\n  console</span>.log('bar')\n}";
//         assert_eq!(apply_highlights(text, &ranges), expected.replace("\n", "<br>"));
//     }

//     #[test]
//     fn test_apply_highlights_out_of_bounds() {
//         let text = "function foo() {\n  console.log('bar')\n}";
//         let ranges = vec![
//             Range { start_line: 0, start_col: 0, end_line: 3, end_col: 8 },
//         ];
//         let expected = "<span class=\"bg-yellow-200\">function foo() {\n  console.log('bar')\n}</span>";
//         assert_eq!(apply_highlights(text, &ranges), expected.replace("\n", "<br>"));
//     }
// }
