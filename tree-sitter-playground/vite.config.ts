import vue from "@vitejs/plugin-vue";
import { defineConfig, type Plugin } from "vite";
import svgLoader from "vite-svg-loader";
import {getLogger} from './src/log'
import wasmPack from './vite_plugins/vite-wasm-pack'
import tailwind from "tailwindcss"
import autoprefixer from "autoprefixer"

function wasmHeaderPlugin(): Plugin {
  return {
    name: "wasm-header",
    configureServer(server) {
      server.middlewares.use((req, res, next) => {
        getLogger().info(`Request URL: ${req.url}`);
        if (req?.url?.endsWith(".wasm")) {
          res.setHeader("Content-Type", "application/wasm");
        }
        next();
      });
    },
  };
}

export default defineConfig({
  plugins: [wasmPack('crates/highlight_wasm'), vue(), svgLoader(), wasmHeaderPlugin()],
  define: {
    global: "window",
  },
  resolve: {
    alias: {
      '@': '/src',
      '@components': '/src/components',
    }
  },
  css: {
    postcss: {
      plugins: [tailwind(), autoprefixer()],
    },
  },
});
