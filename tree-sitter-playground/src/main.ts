import { createApp } from "vue";
import "./assets/index.css";
import App from "./App.vue";
import { createPinia } from "pinia";

import { OhVueIcon, addIcons } from "oh-vue-icons";
import {
  CoSun,
  CoCopy,
  IoMoonOutline,
  BiArrowCounterclockwise,
  HiSolidDotsHorizontal,
  BiChevronCompactDown,
  BiXCircle
} from "oh-vue-icons/icons";

addIcons(
  CoSun,
  IoMoonOutline,
  CoCopy,
  BiArrowCounterclockwise,
  HiSolidDotsHorizontal,
  BiChevronCompactDown,
  BiXCircle
);

const pinia = createPinia();
const app = createApp(App);
app.component("v-icon", OhVueIcon);
app.use(pinia);
app.mount("#app");
