import { getLogger } from "@/log";

interface BaseDBData {
  docType: string;
  _id?: string;
}

export type ParserId = string;

interface UserSettingsDoc extends BaseDBData {
  docType: "user-settings";
  selectedFields: string[];
  parserId: ParserId | undefined;
}

interface QueryInputDoc extends BaseDBData {
  docType: "query-input";
  queryInput: string;
}

interface ParseInputDoc extends BaseDBData {
  docType: "parse-input";
  parseInput: string;
}

interface WasmFileInputDocData extends BaseDBData {
  docType: "wasm-file";
  _id: string;
  _attachments: {
    wasm: {
      content_type: string;
      data: Blob;
    };
  };
  filename: string;
  lastModified: number;
}

interface LastSelectedParser extends BaseDBData {
  docType: "last-selected-parser";
  parser: string;
}

export interface WasmFileOutputDocData {
  docType: "wasm-file";
  name: string;
  filename: string;
  lastModified: number;
  attachment: File;
}

type DBData =
  | UserSettingsDoc
  | QueryInputDoc
  | ParseInputDoc
  | WasmFileInputDocData
  | LastSelectedParser;

interface PouchError {
  /**
   * HTTP Status Code during HTTP or HTTP-like operations
   */
  status?: number | undefined;
  name?: string | undefined;
  message?: string | undefined;
  reason?: string | undefined;
  error?: string | boolean | undefined;
  id?: string | undefined;
}

class LocalDB {
  #logger: ReturnType<typeof getLogger>;
  constructor(public readonly pouch: Awaited<ReturnType<typeof getPouchDB>>) {
    this.#logger = getLogger()
  }

  async saveWasmFile(file: File, name: string) {
    try {
      await this.pouch.put({
        docType: "wasm-file",
        filename: file.name,
        _id: `wasm-file-${name}`,
        _attachments: {
          wasm: {
            content_type: file.type,
            data: file,
          },
        },
        lastModified: new Date().getTime(),
      });
      this.#logger.info(`Saved wasm file ${name}`);
    } catch (e) {
      if ((e as PouchError)?.status !== 409) {
        this.#logger.error(e as Error);
        return;
      }
      await this.pouch.get(`wasm-file-${name}`).then(async (doc) => {
        await this.pouch.remove(doc);
        await this.saveWasmFile(file, name);
      });
    }
  }

  async getWasmFiles(): Promise<WasmFileOutputDocData[]> {
    const docs = await this.pouch.allDocs<WasmFileInputDocData>({
      startkey: "wasm-file-",
      endkey: "wasm-file-\uffff",
      include_docs: true,
    });
    if (!docs.rows.length) {
      return [];
    }
    const files = await Promise.all(
      docs.rows.map(async (row) => {
        const attachment = await this.pouch.getAttachment(row.id, "wasm");
        return {
          name: row.id.replace("wasm-file-", ""),
          attachment: new File([attachment], row.doc?.filename ?? ""),
          filename: row.doc?.filename ?? "",
          lastModified: row.doc?.lastModified ?? 0,
          docType: "wasm-file" as const,
        };
      })
    );
    this.#logger.info(`Got wasm files: ${files.map((f) => f.name).join(", ")}`);
    return files.sort((a, b) => b.lastModified - a.lastModified);
  }

  async deleteWasmFile(name: string) {
    const doc = await this.pouch.get<WasmFileInputDocData>(`wasm-file-${name}`);
    await this.pouch.remove(doc);
    this.#logger.info(`Deleted wasm file ${name}`);
  }
  
  async getUserSettings(): Promise<UserSettingsDoc> {
    try {
      return await this.pouch.get("user-settings");
    } catch (e) {
      if ((e as PouchError)?.status !== 404) {
        this.#logger.error(e as Error);
      }
      return {
        docType: "user-settings",
        _id: "user-settings",
        parserId: undefined,
        selectedFields: [],
      };
    }
  }

  async saveUserSettings(settings: Partial<UserSettingsDoc>) {
    const existingSettings = await this.getUserSettings();
    try {
      await this.pouch.put({
        ...existingSettings,
        ...settings,
        _id: "user-settings",
      });
    } catch (e) {
      if ((e as PouchError)?.status !== 409) {
        this.#logger.error(e as Error);
        return;
      }
      await this.pouch.get<UserSettingsDoc>("user-settings").then(async (doc) => {
        await this.pouch.remove(doc);
        await this.saveUserSettings({
          ...doc,
          ...settings,
        });
      });
    }
  }

}

const getPouchDB = async () => {
  const DB = (await import("pouchdb-browser")).default;
  return new DB<DBData>("localDB");
};

let db: LocalDB;

const getDB = async () => {
  if (!db) {
    db = new LocalDB(await getPouchDB());
  }
  return db;
};

export { getDB };
