import { getDB } from "@/local_db/db";
import { getLogger } from "@/log";
import Parser from "web-tree-sitter";

export const initParser = async () => {
  await Parser.init({
    locateFile: () => "wasm/tree-sitter.wasm",
  });
};

export const preBuiltGrammarIds = ["javascript", "typescript"];
export const getParserForWasmFile = async (wasmFile: File): Promise<Parser> => {
  console.log('niki testing', typeof wasmFile)
  await Parser.init({ locateFile: () => "wasm/tree-sitter.wasm" });
  const newParser = new Parser();

  const reader = new FileReader();
  const loadLanguage = new Promise<void>((resolve, reject) => {
    reader.onload = async () => {
      const wasmBuffer = reader.result as ArrayBuffer;
      try {
        const language = await Parser.Language.load(new Uint8Array(wasmBuffer));
        newParser.setLanguage(language);
        resolve();
      } catch (error) {
        reject(error);
      }
    };
    reader.onerror = () => {
      reject(new Error("Failed to read WASM file."));
    };
  });

  reader.readAsArrayBuffer(wasmFile);
  await loadLanguage;

  return newParser;
};

export const getParserForPreBuiltGrammar = async (
  grammar: string
): Promise<Parser> => {
  const response = await fetch(`/wasm/tree-sitter-${grammar}.wasm`);
  const wasmBuffer = await response.arrayBuffer();
  const wasmFile = new File([wasmBuffer], `tree-sitter-${grammar}.wasm`);
  return getParserForWasmFile(wasmFile);
};

export const getParserForId = async (parserId: string): Promise<Parser> => {
  const logger = getLogger();
  if (preBuiltGrammarIds.includes(parserId)) {
    logger.info(`Using pre-built grammar for ${parserId}`);
    return getParserForPreBuiltGrammar(parserId);
  }
  const db = await getDB();
  const wasmFiles = await db.getWasmFiles();
  const wasmFile = wasmFiles?.find((file) => file.name === parserId);
  if (wasmFile) {
    logger.info(`Using user wasm file for ${parserId}`);
    return getParserForWasmFile(wasmFile.attachment);
  }
  logger.info(`Using default parser for ${parserId}`);
  return new Parser();
};
