import { defineStore, acceptHMRUpdate } from "pinia";
import { getLogger } from "../log";
import { getDB } from "../local_db/db";
import Parser from "web-tree-sitter";
import {
  getParserForId,
  getParserForPreBuiltGrammar,
  getParserForWasmFile,
  initParser,
  preBuiltGrammarIds,
} from "@/components/parser/parser";

const THEME_KEY = "theme";

type Theme = "dark" | "light";

const setTheme = (theme: Theme) => {
  const logger = getLogger();
  logger.info(`Setting theme to ${theme}`);
  localStorage.setItem(THEME_KEY, theme);
  document.body.classList.remove("dark", "light");
  document.body.classList.add(theme);
};

export const useUserStore = defineStore({
  id: "user",
  state: () => {
    const theme =
      (localStorage.getItem(THEME_KEY) as undefined | Theme) ||
      ("light" as const);
    setTheme(theme);
    const parserId = "";
    const parser = {} as Parser;
    return {
      theme,
      parser,
      parserId,
      queryFields: ["name", "text", "type", "startPosition", "endPosition"],
    };
  },
  getters: {
    darkMode: (state) => state.theme === "dark",
  },
  actions: {
    async initialize() {
      const db = await getDB();
      const [currentSettings] = await Promise.all([
        db.getUserSettings(),
        initParser(),
      ]);
      const logger = getLogger();
      logger.info(
        `initializing user store with settings: ${JSON.stringify(
          currentSettings
        )}`
      );
      this.parserId = currentSettings.parserId ?? "";
      if (currentSettings.parserId) {
        this.parser = await getParserForId(currentSettings.parserId);
      }
    },
    toggleDarkMode() {
      const theme = this.theme === "dark" ? "light" : "dark";
      this.theme = theme;
      setTheme(theme);
    },
    async setParser({
      parserId,
      parser,
    }: {
      parserId: string;
      parser: Parser;
    }) {
      this.parserId = parserId;
      this.parser = parser;
      const db = await getDB();
      await db.saveUserSettings({ parserId });
    },
  },
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useUserStore, import.meta.hot));
}
