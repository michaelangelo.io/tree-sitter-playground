import dayjs from 'dayjs';


export interface DetailedError extends Error {
  readonly details: Record<string, unknown>;
}

export function isDetailedError(object: unknown): object is DetailedError {
  return Boolean((object as DetailedError).details);
}

export const prettyJson = (
  obj: Record<string, unknown> | unknown[],
  space: string | number = 2,
): string => JSON.stringify(obj, null, space);


export interface ILog {
  debug(e: Error): void;
  debug(message: string, e?: Error): void;
  info(e: Error): void;
  info(message: string, e?: Error): void;
  warn(e: Error): void;
  warn(message: string, e?: Error): void;
  error(e: Error): void;
  error(message: string, e?: Error): void;
}

export const LOG_LEVEL = {
  DEBUG: 'debug',
  INFO: 'info',
  WARNING: 'warning',
  ERROR: 'error',
} as const;

export type LogLevel = (typeof LOG_LEVEL)[keyof typeof LOG_LEVEL];

const LOG_LEVEL_MAPPING = {
  [LOG_LEVEL.ERROR]: 1,
  [LOG_LEVEL.WARNING]: 2,
  [LOG_LEVEL.INFO]: 3,
  [LOG_LEVEL.DEBUG]: 4,
};

const getNumericMapping = (logLevel: keyof typeof LOG_LEVEL_MAPPING | undefined) => {
  if (logLevel && logLevel in LOG_LEVEL_MAPPING) {
    return LOG_LEVEL_MAPPING[logLevel];
  }
  // Log level could be set as an invalid string by an LS client, or the .
  return LOG_LEVEL_MAPPING[LOG_LEVEL.INFO];
};

// pad subsequent lines by 4 spaces
const PADDING = 4;

const multilineLog = (line: string, level: LogLevel): void => {
  const prefix = `${dayjs().format('YYYY-MM-DDTHH:mm:ss:SSS')} [${level}]: `;
  const padNextLines = (text: string) => text.replace(/\n/g, `\n${' '.repeat(PADDING)}`);

  console.log(`${prefix}${padNextLines(line)}`);
};

const formatError = (e: Error): string =>
  isDetailedError(e) ? prettyJson(e.details) : `${e.message}\n${e.stack}`;

const ensureError = (e: unknown): Error | undefined => {
  if (e instanceof Error) {
    return e;
  }if (typeof e === 'string') {
    return new Error(e);
  }if (e === undefined || e === null) {
    return undefined;
  }

  try {
    return new Error(JSON.stringify(e));
  } catch (_) {
    return undefined;
  }
};

const logWithLevel = (level: LogLevel, a1: Error | string, a2?: Error) => {
  if (typeof a1 === 'string') {
    const errorText = a2 ? `\n${formatError(a2)}` : '';
    multilineLog(`${a1}${errorText}`, level);
  } else {
    multilineLog(formatError(a1), level);
  }
};

class Log implements ILog {

  #level: LogLevel = LOG_LEVEL.INFO;

  constructor(level?: LogLevel) {
    this.#level = level || LOG_LEVEL.INFO;
  }

  /**
   * @param messageOrError can be error (if we don't want to provide any additional info), or a string message
   * @param trailingError is an optional error (if messageOrError was a message)
   *           but we also mention `unknown` type because JS doesn't guarantee that in `catch(e)`,
   *           the `e` is an `Error`, it can be anything.
   * */
  #log(
    incomingLogLevel: LogLevel,
    messageOrError: Error | string,
    trailingError?: Error | unknown,
  ) {
    const shouldShowLog = getNumericMapping(this.#level) >= LOG_LEVEL_MAPPING[incomingLogLevel];
    if (shouldShowLog) {
      logWithLevel(incomingLogLevel, messageOrError, ensureError(trailingError));
    }
  }

  debug(messageOrError: Error | string, trailingError?: Error | unknown) {
    this.#log(LOG_LEVEL.DEBUG, messageOrError, trailingError);
  }
  info(messageOrError: Error | string, trailingError?: Error | unknown) {
    this.#log(LOG_LEVEL.INFO, messageOrError, trailingError);
  }
  warn(messageOrError: Error | string, trailingError?: Error | unknown) {
    this.#log(LOG_LEVEL.WARNING, messageOrError, trailingError);
  }
  error(messageOrError: Error | string, trailingError?: Error | unknown) {
    this.#log(LOG_LEVEL.ERROR, messageOrError, trailingError);
  }

  setLevel(level: LogLevel) {
    this.#level = level;
  }
}

let logInstance: Log;

export const getLogger = (level?: LogLevel): ILog => {
  if (!logInstance) {
    logInstance = new Log(level);
  }
  return logInstance;
}
