FROM node:22.2.0-alpine

WORKDIR /app

COPY package.json pnpm-lock.yaml ./

RUN apk add --no-cache python3 make g++ curl

RUN npm install -g pnpm && pnpm install --frozen-lockfile

COPY . .

RUN pnpm build

EXPOSE 3000

CMD ["node", ".output/server/index.mjs"]